'use strict'
const mongoose=require('mongoose'),
Schema = mongoose.Schema,
ObjectId = Schema.ObjectId;

mongoose.connect('mongodb://localhost/events');

const db=mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log('we\'re connected!')
});

var Event=mongoose.model('Event', new Schema({
	id: ObjectId,
	date: 				{type: Date},
	cost: 				{type: Number},
	duration:			{type: Number},
	mc:					{type: String},
	address: 			{type: String},
	language: 			{type: String},
	linkToDescrEvType:	{type: String},

	eventType: {
		ru: 			{type: String},
		en: 			{type: String},
		fr: 			{type: String}
	},
	shortDescr:{
		ru: 			{type: String},
		en: 			{type: String},
		fr: 			{type: String}
	},
	description: {
		ru: 			{type: String},
		en: 			{type: String},
		fr: 			{type: String}
	}

}));
module.exports.EVent=Event

module.exports.addEvent=(req,res)=>{
	var event=new Event({
	date: 				req.body.date,
	cost: 				req.body.cost,
	duration:			req.body.duration,
	mc:					req.body.mc,
	address: 			req.body.address,
	language: 			req.body.lang,
	
	eventType: {
		ru: 			req.body.event_type,
		en: 			req.body.event_type_en,
		fr: 			req.body.event_type_fr
	},
	shortDescr:{
		ru: 			req.body.short_description,
		en: 			req.body.short_description_en,
		fr: 			req.body.short_description_fr
	},
	description: {
		ru: 			req.body.descr,
		en: 			req.body.descr_en,
		fr: 			req.body.descr_fr
	}
	})
	console.log('event->'+event)

	event.save((err)=>{
		if (err) {
			console.log(err)}
		else {
		// utils.createUserSession(req, res, user);
		res.send('event'+req.body.event_type_en+ 'has been successfully created')}
	}) 
}

module.exports.eventsPage=function(req,res){
Event.find({},function(err,events){
	var result
	if (err) {
		console.log(err) 
	}
	else{

		console.log( typeof(events))
		console.log('events-> '+events)
		result=events 
	}
	console.log(result)
	return result
})
}
//сделать linkToDescrEvType: автокомплитом 
