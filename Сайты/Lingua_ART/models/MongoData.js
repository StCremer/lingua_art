'use strict'
var mongoose=require('mongoose'),
Schema = mongoose.Schema,
ObjectId = Schema.ObjectId;

/**
 * Our User model.
 *
 * This is how we create, edit, delete, and retrieve user accounts via MongoDB.
 */
// module.exports.User = mongoose.model('User', new Schema({
//   id:           ObjectId,
//   login: 		{type: String,required: '{PATH} is required.', unique:true},
//   firstName:    { type: String, required: '{PATH} is required.' },
//   lastName:     { type: String },
//   email:        { type: String, required: '{PATH} is required.', unique: true },
//   password:     { type: String, required: '{PATH} is required.' },
//   data:         Object,
// }));


