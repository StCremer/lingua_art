'use strict';
var express= require('express'),
http=require ('http'),
routes= require('./routes'),
app= express(),
util=require('util'),
favicon= require('serve-favicon'),
morgan= require('morgan'),
path= require('path'),
// redis= require('redis'),
session = require('express-session'),

RedisStore=require('connect-redis')(session),
bodyParser = require('body-parser'),
// async = require('async'),
// data={},
server;

app.disable('x-powered-by');

app.set('views', path.join(__dirname, 'views'));
app.set('view engine','jade');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));
// app.use(favicon(__dirname +'/public/images/ico2.png'));
app.use(morgan('dev'));
app.use(session({
	name: 'Lingia[ART]',
	store: new RedisStore({
		// host: '127.0.0.1',
		db:1
	}),
    secret: 'xfgncfgncfgn'
}));

app.use(require('./routes/events'))
// app.use(require('./routes/user'))



server = http.createServer(app).listen(8888,function(){

	console.log('server running at port 8888');
});

// 