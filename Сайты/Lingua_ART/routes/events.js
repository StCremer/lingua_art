'use strict'
const express=require('express'),
events=require('../models/events.js'),
Event=events.EVent,
mongoose=require('mongoose'),
router=express.Router()

router.get('/',function(req,res){
	res.redirect('/events')
})

router.get('/events',function(req,res){
	Event.find({},function(err,events){
		var result
		if (err) {
			console.log(err) 
		}
		else{

			console.log( typeof(events))
			console.log('events-> '+events)
			res.render('Events.jade',{events: events}) 
		}
	})
	}

	// function(req,res){
	// res.render('Events.jade',events.eventsPage)
// }
)

router.get('/new-event',(req,res)=>{
	res.render('new_event.jade',{page_name:'new-event'})
})

router.post('/new-event',events.addEvent)

router.get('/event',(req,res)=>{
	res.render('event.jade',{page_name:'event'})

})


module.exports=router